/**
 * @file contains entry point of controllers module
 * @author Sri Handayani
 */

const api = require("./api");
const main = require("./main");

module.exports = {
  api,
  main,
};
