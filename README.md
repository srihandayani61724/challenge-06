

## Database Management

pada repository ini sudah terdapat beberapa script yang dapat digunakan untuk memanage database, yaitu:

- `yarn db:create` : membuat database
- `yarn db:drop` : menghapus database
- `yarn db:migrate` : menjalankan database migration
- `yarn db:seed` : melakukan seeding
- `yarn db:rollback` : migrasi terakhir
